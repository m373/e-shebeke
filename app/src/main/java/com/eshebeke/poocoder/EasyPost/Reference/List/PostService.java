package com.eshebeke.poocoder.EasyPost.Reference.List;

/**
 * Created by User on 25/11/2017.
 */


public class PostService {
    private String title, genre, year;


    public PostService(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}