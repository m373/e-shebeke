package com.eshebeke.poocoder.EasyPost.Reference;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eshebeke.poocoder.EasyPost.Reference.List.PostService;
import com.eshebeke.poocoder.EasyPost.Reference.List.PostServiceAdapter;
import com.eshebeke.poocoder.EasyPost.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by poocoder on 11/22/17.
 */

public class PostServicesFragment extends Fragment {
    public List<PostService> postServiceList = new ArrayList<>();
    public RecyclerView recyclerView;
    public PostServiceAdapter mAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View postServicesView = inflater.inflate(R.layout.postservices, null);

        recyclerView = (RecyclerView) postServicesView.findViewById(R.id.recycler_view);

        mAdapter = new PostServiceAdapter(postServiceList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        recyclerView.setAdapter(mAdapter);

        prepareMovieData();

        return postServicesView;
    }

    private void prepareMovieData() {
        PostService postService = new PostService("Bestcom şirkətinin  xalq kompyuterlərin müştərilərə çatdırılması və kopyuterlərin kredit haqlarının qəbulu");
        postServiceList.add(postService);

        postService = new PostService("Səba bank olmayan kredit təşkilatının  kredit haqlarını qəbulu");
        postServiceList.add(postService);

        postService = new PostService("Yapı Kredit bankın pin kodların debet və kredit kartların çatdırılması və kreditlərin qəbulu");
        postServiceList.add(postService);

        postService = new PostService("Unibank-ın kredit haqlarının qəbulu");
        postServiceList.add(postService);

        postService = new PostService("DSMF-nun məcburi dövlət sosial  sığorta ödənişlərinin qəbulu");
        postServiceList.add(postService);

        postService = new PostService("Vergilər Nazirliyinin vergi, rüsum  ödənişlərin qəbulu");
        postServiceList.add(postService);

        postService = new PostService("Bələdiyyələrin vergi ödənişlərinin qəbulu");
        postServiceList.add(postService);

        postService = new PostService("Azərişıq”ASC-nin,“Azərqaz”İB-nin və “Azərsu” ASC-nin (smart kartlar istisna olmaqla) abunəçilərindən elektrik enerjisi, təbii qaz, su və kanalizasiya haqlarının qəbulu");
        postServiceList.add(postService);

        postService = new PostService("Dövlət yol polisinin cərimələrinin qəbulu");
        postServiceList.add(postService);

        postService = new PostService("İcbari sığorta şəhadətnaməlrinin satışı");
        postServiceList.add(postService);

        postService = new PostService("Beynəlxalq  pul köçürmələrinin həyata keçirilməsi");
        postServiceList.add(postService);

        postService = new PostService("Respublika daxili pul köçürmələrinin həyata keçirilməsi");
        postServiceList.add(postService);

        postService = new PostService("Valyuta mübadiləsi");
        postServiceList.add(postService);

        postService = new PostService("Plastik kart satışı");
        postServiceList.add(postService);

        postService = new PostService("Mobil cihazların İMEİ kodunun qeydə alınması");
        postServiceList.add(postService);

        postService = new PostService("Poçt hesablarının açılması və aparılması");
        postServiceList.add(postService);

        postService = new PostService("Respublika daxili  məktub, banderol, bağlamaların qəbulu və çatdırılması");
        postServiceList.add(postService);

        postService = new PostService("Beynəlxalq məktub, banderol, bağlamaların qəbulu və çatdırılması");
        postServiceList.add(postService);

        postService = new PostService("Respubilka daxili  teleqramların qəbulu və çatdırılması");
        postServiceList.add(postService);

        postService = new PostService("Beynəlxalq teleqramların qəbulu və çatdırılması");
        postServiceList.add(postService);

        postService = new PostService("Xüsusi  təinatlı poçt göndərişlərinin qəbulu və çatdırılması");
        postServiceList.add(postService);

        postService = new PostService("Əsgəri poçt göndərişlərinin qəbulu və çatdırılması");
        postServiceList.add(postService);

        postService = new PostService("EMS  və  Kuryer  poçt  xidmətləri");
        postServiceList.add(postService);

        postService = new PostService("Dövri mətbuatın abunəsi, çatdırılması və satışı");
        postServiceList.add(postService);

        postService = new PostService("Müxtəlif çeşiddə və ölçüdə açıqca, marka, zərf satışı");
        postServiceList.add(postService);

        postService = new PostService("Elektron imza xidmətlərinin qəbulu");
        postServiceList.add(postService);

        postService = new PostService("Elektron Hökumət Portalı xidmətlərinin göstərilməsi");
        postServiceList.add(postService);

        postService = new PostService("Ünvanlı dövlət sosial yardım xidmətlərinin göstərilməsi");
        postServiceList.add(postService);

        postService = new PostService("Azəristiliktəchizat ASC-nin xidmət haqlarının qəbulu");
        postServiceList.add(postService);

        postService = new PostService("Aztelkom” MMC-nin və “BTRİB”- nin abunəçilərindən şəhərlərarası və beynəlxalq telefon danışıq, aylıq abunə haqlarının qəbulu və qəbzlərin çatdırılması");
        postServiceList.add(postService);

        postService = new PostService("TransEvroKom” MMC üzrə ŞATD və abunə haqlarının qəbulu");
        postServiceList.add(postService);

        postService = new PostService("Aztelkom” MMC-nin,“BTRİB”-nin və MHM-nin abunəçilərindən internet haqlarının qəbulu");
        postServiceList.add(postService);

        postService = new PostService("Azərcell”, “Bakcell”, “Katell”, “Ultell” və “Azerfon-Vodafone”-nun ödənişlərinin qəbulu və Azərcell-nin abonentlərinə fakturaların  çatdırılması\"");
        postServiceList.add(postService);

        postService = new PostService("Connect TV, B&B TV və ADSL internet ödənişlərinin (“Komtec Ltd” MMC-nin sistemi vasitəsilə)  qəbulu göstərilməsi");
        postServiceList.add(postService);

        postService = new PostService("Respublika daxili və beynəlxalq istiqamətlərə aviabiletlərin satışı");
        postServiceList.add(postService);

        postService = new PostService("Distan Hazırlıq Kurs” MMC-nin müştərilərindən abunə haqlarının qəbulu");
        postServiceList.add(postService);

        postService = new PostService("Abunə qutularının sifarişlərinin qəbulu və satışı");
        postServiceList.add(postService);

        postService = new PostService("Faks və kserekopiya xidmətlərinin göstərilməsi");
        postServiceList.add(postService);

        mAdapter.notifyDataSetChanged();
    }

}
