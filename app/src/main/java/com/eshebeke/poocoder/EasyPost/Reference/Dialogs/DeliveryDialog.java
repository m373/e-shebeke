package com.eshebeke.poocoder.EasyPost.Reference.Dialogs;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TimePicker;

import com.eshebeke.poocoder.EasyPost.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by poocoder on 11/25/17.
 */

public class DeliveryDialog extends DialogFragment implements View.OnClickListener{

    Button setDateTime;

    EditText date;
    EditText time;
    EditText location;
    Switch payment;
    boolean takeMyself = true;
    boolean isPayed = false;
    boolean islocation = false;
    Calendar calendar;

    String dateS, timeS;

    private FirebaseAuth mAuth;


    AdapterView<?> parcelParent;
    View parcelView;
    int parcelPosition;
    long parcelId;

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.location:
                islocation = true;
                break;
            case R.id.time:
                TimePickerDialog tp = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        timeS = hourOfDay + ":" + minute;
                       time.setText(timeS);
                    }
                }, 0, 0 , true);

                tp.show();

                break;

            case R.id.date:
                DatePickerDialog dp = new DatePickerDialog(
                getActivity(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        dateS = dateFormatter.format(newDate.getTime());
                        date.setText(dateS);
                    }
                    }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                dp.show();

                break;

            case R.id.set:
                dateListener.onCategoryName(dateS +" " + timeS,location.getText().toString(), parcelPosition, takeMyself, isPayed, islocation);
                dismiss();

    }
    }


    public interface OnDateListener{
        void onCategoryName(String date,String address, int position,boolean yourself, boolean paid, boolean locationChanged);
    }

    private DeliveryDialog.OnDateListener dateListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try{
            this.dateListener = (DeliveryDialog.OnDateListener) context;
        }
        catch (final ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnCompleteListener");
        }
    }

    @SuppressLint("ValidFragment")
    DeliveryDialog(AdapterView<?> parent, View view, int position, long id, boolean takeMyself) {
        parcelView = view;
        parcelParent = parent;
        parcelPosition = position;
        parcelId = id;
        this.takeMyself = takeMyself;
    }

    public DeliveryDialog(){

    }




    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle(R.string.pickadate);
        View v = inflater.inflate(R.layout.shipping_info_layout, null);

        mAuth = FirebaseAuth.getInstance();
        calendar = Calendar.getInstance();



        location = v.findViewById(R.id.location);
        time = v.findViewById(R.id.time);
        date = v.findViewById(R.id.date);
        payment = v.findViewById(R.id.payment);
        setDateTime = v.findViewById(R.id.set);

        if(!takeMyself){
            location.setVisibility(View.GONE);
        }

        location.setOnClickListener(this);
        date.setOnClickListener(this);
        time.setOnClickListener(this);

        setDateTime.setOnClickListener(this);

        payment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isPayed = isChecked;
            }
        });


        DatabaseReference refname = FirebaseDatabase.getInstance().getReference();
        refname.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                try {
                    String address = snapshot.child("user").child(mAuth.getCurrentUser().getUid()).child("postalAddressS").getValue(String.class);

                    location.setText(address);
                }catch (NullPointerException e){
                    Log.d("UID", "UID is null");
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return v;
    }


}
