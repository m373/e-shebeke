package com.eshebeke.poocoder.EasyPost;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by poocoder on 11/25/17.
 */

@IgnoreExtraProperties
public class ObtainedParcel {
    private String Address;

    public String getPayment() {
        return Payment;
    }

    public void setPayment(String payment) {
        Payment = payment;
    }

    private String Payment;
    private String Date;
    private String Delivery;
    private String Price;
    private String Weight;

    public ObtainedParcel(String Address,String Date, String Delivery, String Payment, String Price, String Weight){

        this.Address = Address;

        this.Price = Price;
        this.Weight = Weight;
        this.Date = Date;
        this.Delivery = Delivery;
        this.Payment = Payment;
    }

    public ObtainedParcel(){

    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getDelivery() {
        return Delivery;
    }

    public void setDelivery(String delivery) {
        Delivery = delivery;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }
}
