package com.eshebeke.poocoder.EasyPost.Account;

import com.eshebeke.poocoder.EasyPost.Reference.List.Parcel;

import java.util.ArrayList;

/**
 * Created by poocoder on 11/24/17.
 */


public class User {
    private String nameS, postalAddressS, phoneNumberS, emailS;
    private ArrayList<Parcel> parcels;
    private ArrayList<Parcel> parcelsHistory;

    public ArrayList<Parcel> getParcels() {
        return parcels;
    }

    public void setParcels(ArrayList<Parcel> parcels) {
        this.parcels = parcels;
    }

    public ArrayList<Parcel> getParcelsHistory() {
        return parcelsHistory;
    }

    public void setParcelsHistory(ArrayList<Parcel> parcelsHistory) {
        this.parcelsHistory = parcelsHistory;
    }

    public User(String nameS, String postalAddressS, String phoneNumberS, String emailS, ArrayList<Parcel> parcels,
                ArrayList<Parcel> parcelsHistory){
        this.nameS = nameS;
        this.phoneNumberS = phoneNumberS;
        this.postalAddressS = postalAddressS;
        this.emailS = emailS;
        this.parcels = parcels;
        this.parcelsHistory = parcelsHistory;
    }

    public String getNameS() {
        return nameS;
    }

    public String getPostalAddressS() {
        return postalAddressS;
    }

    public String getPhoneNumberS() {
        return phoneNumberS;
    }

    public void setNameS(String nameS) {
        this.nameS = nameS;
    }

    public String getEmailS() {
        return emailS;
    }

    public void setEmailS(String emailS) {
        this.emailS = emailS;
    }
}
