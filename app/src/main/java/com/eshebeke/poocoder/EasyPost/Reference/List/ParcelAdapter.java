package com.eshebeke.poocoder.EasyPost.Reference.List;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.eshebeke.poocoder.EasyPost.R;

import java.util.ArrayList;

/**
 * Created by poocoder on 11/22/17.
 */

public class ParcelAdapter extends ArrayAdapter<Parcel> {
    public ArrayList<Parcel> parcelSet;
    Context mContext;

    static class ViewHolder{
        TextView trackingNumber;
        TextView appointmentTime;
        ImageView location, paid, shipment;
    }


    public ParcelAdapter(@NonNull Context context, ArrayList<Parcel> objects) {
        super(context, R.layout.parcel_item, objects);
        mContext = context;
        parcelSet = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final Parcel parcel = getItem(position);
        View result;
        ViewHolder viewHolder;

        if(convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.parcel_item, parent, false);

            viewHolder.trackingNumber = convertView.findViewById(R.id.tracking_number);
            viewHolder.appointmentTime = convertView.findViewById(R.id.appointment_time);
            viewHolder.location = convertView.findViewById(R.id.isshipped);
            viewHolder.paid = convertView.findViewById(R.id.ispaid);
            viewHolder.shipment = convertView.findViewById(R.id.isdefaultlocation);

            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }


        viewHolder.appointmentTime.setText(parcel.getAppointmentDate());
        viewHolder.trackingNumber.setText(parcel.getTrackingNum());
        viewHolder.appointmentTime.setText(parcel.getAppointmentDate());


        if(parcel.isChangedDeliveryPlace())
            viewHolder.shipment.setVisibility(View.VISIBLE);


        return result;
    }
}
