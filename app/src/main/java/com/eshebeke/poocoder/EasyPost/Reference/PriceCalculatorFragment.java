package com.eshebeke.poocoder.EasyPost.Reference;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.eshebeke.poocoder.EasyPost.R;
import com.jaredrummler.materialspinner.MaterialSpinner;

/**
 * Created by poocoder on 11/22/17.
 */

public class PriceCalculatorFragment extends Fragment {
    MaterialSpinner deliveryTypeSpinner,destinationTypeSpinner;
    EditText deliveryWeight;
    TextView price;
    Button calculateButton;
    String selectedDeliveryType,selectedDestination=null;

    View calcView;

    String delivery_types[]={
            "Çəkisi 2 kiloqramdan yuxarı banderol",
            "Qiyməti bəyan olunmuş göndəriş (məktub və banderol)",
            "Ağırçəkili (10 kq-dan artıq), qiyməti bəyan olunmuş, kövrək və ya iriölçülü bağlama",

    };

    String destination_types[]={
            "Daxili",
            "Beynəlxalq"

    };


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        calcView = inflater.inflate(R.layout.activity_price_calculator, null);

        deliveryTypeSpinner=(MaterialSpinner)calcView.findViewById(R.id.delivery_type);
        destinationTypeSpinner=(MaterialSpinner)calcView.findViewById(R.id.destination_type);
        deliveryWeight=(EditText)calcView.findViewById(R.id.weight_input);
        price=(TextView)calcView.findViewById(R.id.price_output);
        calculateButton=(Button)calcView.findViewById(R.id.calculate_button);

        selectedDeliveryType=delivery_types[0];
        selectedDestination=destination_types[0];

        destinationTypeSpinner.setItems(destination_types);
        deliveryTypeSpinner.setItems(delivery_types);


        deliveryTypeSpinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                selectedDeliveryType=item.toString();
            }
        });

        destinationTypeSpinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                selectedDestination=item.toString();
            }
        });


        calculateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedDeliveryType!=null && selectedDestination!=null && !deliveryWeight.getText().toString().equals("")){
                    int weight=Integer.valueOf(deliveryWeight.getText().toString());
                    if(weight<2000){
                        Toast.makeText(getActivity(), "Must be heavier than 2 kg", Toast.LENGTH_SHORT).show();
                    }else if(weight>2000 && weight <3000){
                        price.setText("2.4 AZN");
                    }else if(3000<weight && 4000>weight){
                        price.setText("3.0 AZN");
                    }else if(4000<weight && 5000>weight){
                        price.setText("3.6 AZN");
                    }else{
                        price.setText("5 AZN");
                    }

                }
            }
        });


        return calcView;
    }
}
