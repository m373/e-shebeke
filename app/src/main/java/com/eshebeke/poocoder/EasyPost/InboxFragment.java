package com.eshebeke.poocoder.EasyPost;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.eshebeke.poocoder.EasyPost.Reference.List.Parcel;
import com.eshebeke.poocoder.EasyPost.Reference.List.ParcelAdapter;
import com.eshebeke.poocoder.EasyPost.ObtainedParcel;
import com.eshebeke.poocoder.EasyPost.Reference.Dialogs.ParcelActionsDialog;
import com.eshebeke.poocoder.EasyPost.R;
import com.eshebeke.poocoder.EasyPost.Reference.Dialogs.DeliveryDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Created by poocoder on 12/1/17.
 */

public class InboxFragment extends Fragment implements
        AdapterView.OnItemClickListener, DeliveryDialog.OnDateListener {
    View v;
    private FirebaseAuth mAuth;
    public FirebaseAuth.AuthStateListener mAuthListener;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    boolean loggedin = false;

    ArrayList<Parcel> parcelSet;
    ListView parcelList;
    ParcelAdapter parcelAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.inbox_layout, null);

        mAuth = FirebaseAuth.getInstance();

        parcelSet = new ArrayList<>();
        parcelList = v.findViewById(R.id.parcel_list);
        parcelAdapter = new ParcelAdapter(getActivity(), parcelSet);
        parcelList.setOnItemClickListener(this);
        parcelList.setAdapter(parcelAdapter);


        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                FirebaseUser user = firebaseAuth.getCurrentUser();

                if (user == null) {
                    dataBaseLogin(null);
                    parcelSet.clear();
                    parcelAdapter.notifyDataSetChanged();

                    /*
                    hideItem(navigationView, R.id.nav_logout);
                    showItem(navigationView, R.id.nav_sign_up);
                    showItem(navigationView, R.id.nav_login);
*/

                    loggedin = false;
                } else {
                    dataBaseLogin(user);
                    try {
                        Toast.makeText(getActivity(), "Username:" + mAuth.getCurrentUser().getEmail(), Toast.LENGTH_SHORT).show();
                    }catch (NullPointerException e){
                        Log.d("Firebase", "Username is null");
                    }
                    /*hideItem(navigationView, R.id.nav_login);
                    hideItem(navigationView, R.id.nav_sign_up);
                    showItem(navigationView, R.id.nav_logout);*/
                    firebaseAuthSync();

                }
            }
        };


        mAuth.addAuthStateListener(mAuthListener);


        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();

    }

    public void firebaseAuthSync() {

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();


        // Read from the database
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // String fbname = dataSnapshot.child("user").child("id").child("nameS").getValue(String.class);

                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                if (mAuth.getCurrentUser() != null) {

                    Iterable<DataSnapshot> obtainedParcels = dataSnapshot.child("user").child(mAuth.getCurrentUser()
                            .getUid()).child("aparcel").getChildren();

                    parcelSet.clear();

                    if (!obtainedParcels.iterator().hasNext()) {
                        parcelAdapter.notifyDataSetChanged();
                    }

                    while (obtainedParcels.iterator().hasNext()) {
                        DataSnapshot ds = obtainedParcels.iterator().next();
                        ObtainedParcel ci = ds.getValue(ObtainedParcel.class);
                        try {
                            parcelSet.add(new Parcel(ds.getKey(), ci.getDate(), !ci.getDelivery().equals("false"), ci.getPayment().equals("true")));
                        }catch (NullPointerException e){
                            Log.d("Firebase DataSnapshot", "Some property is null");
                        }
                    }

                    parcelAdapter.notifyDataSetChanged();
                }


            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("Firebase", "Failed to read value.", error.toException());
            }
        });
    }

    public void dataBaseLogin(FirebaseUser user) {
        // Check if user is signed in (non-null) and update UI accordingly.

        if (user != null) {
            firebaseAuthSync();
            loggedin = true;
        } else {
            loggedin = false;
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Log.d("TESTING", position + "");
        ParcelActionsDialog actionsDialog = new ParcelActionsDialog(parent, view, position, id);
        actionsDialog.show(getActivity().getFragmentManager(), "Choose the action");
    }


    @Override
    public void onCategoryName(String date, String address, int position, boolean yourself, boolean paid, boolean islocation) {
        if(mAuth.getCurrentUser().getUid() != null) {
            databaseReference.child("user").child(mAuth.getCurrentUser().getUid())
                    .child("aparcel")
                    .child(parcelSet.get(position).getTrackingNum()).child("Payment")
                    .setValue(Boolean.toString(paid));
            databaseReference.child("user").child(mAuth.getCurrentUser().getUid())
                    .child("aparcel")
                    .child(parcelSet.get(position).getTrackingNum()).child("Date")
                    .setValue(date);
            databaseReference.child("user").child(mAuth.getCurrentUser().getUid())
                    .child("aparcel")
                    .child(parcelSet.get(position).getTrackingNum()).child("Address")
                    .setValue(address);
            databaseReference.child("user").child(mAuth.getCurrentUser().getUid())
                    .child("aparcel")
                    .child(parcelSet.get(position).getTrackingNum()).child("Delivery")
                    .setValue(Boolean.toString(yourself));

        }else{
            Toast.makeText(getActivity(), "Please Log In", Toast.LENGTH_SHORT).show();
        }
        Log.d("TESTING", position + " " + date);

        parcelAdapter.notifyDataSetChanged();

    }
}
