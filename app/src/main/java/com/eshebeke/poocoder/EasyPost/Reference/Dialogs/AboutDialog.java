package com.eshebeke.poocoder.EasyPost.Reference.Dialogs;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eshebeke.poocoder.EasyPost.R;

/**
 * Created by poocoder on 11/26/17.
 */

public class AboutDialog extends DialogFragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle(R.string.about);
        return(inflater.inflate(R.layout.about_layout, null));


    }
}
