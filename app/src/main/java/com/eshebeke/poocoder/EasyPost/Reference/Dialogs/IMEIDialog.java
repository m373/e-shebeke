package com.eshebeke.poocoder.EasyPost.Reference.Dialogs;

import android.app.DialogFragment;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.eshebeke.poocoder.EasyPost.R;

/**
 * Created by poocoder on 11/26/17.
 */

public class IMEIDialog extends DialogFragment {

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle(R.string.imei);
        View v = inflater.inflate(R.layout.imei_layout, null);

        final Button search = v.findViewById(R.id.searchImei);
        final EditText imeitext = v.findViewById(R.id.imei_view);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "The number: " + imeitext.getText()+ " is registered!", Toast.LENGTH_SHORT).show();
            }
        });


        return v;
    }
}
