package com.eshebeke.poocoder.EasyPost.Account;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.eshebeke.poocoder.EasyPost.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


/*
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
*/

/**
 * Created by poocoder on 11/16/17.
 */

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener{
    EditText name, password, confirmPassword, email, postalAddress,phoneNumber;
    String nameS, passwordS, confirmPasswordS, emailS, postalAddressS, phoneNumberS;
    FirebaseAuth mAuth;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference firebaseReference;
    FirebaseAuth.AuthStateListener mAuthListener;


    Button register;
    final static String TAG = "Login_Status";

    //FirebaseAuth mAuth;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_signup);
        mAuth = FirebaseAuth.getInstance();


        name = findViewById(R.id.input_name);
        postalAddress = findViewById(R.id.postalAddress);
        password = findViewById(R.id.input_password);
        email = findViewById(R.id.input_email);
        confirmPassword = findViewById(R.id.input_reEnterPassword);
        register = findViewById(R.id.btn_signup);
        phoneNumber = findViewById(R.id.input_mobile);




        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if(user != null){
                    User userm = new User(nameS, postalAddressS, phoneNumberS, emailS, null, null);
                    firebaseDatabase = FirebaseDatabase.getInstance();
                    firebaseReference = firebaseDatabase.getReference();

                    if(mAuth.getCurrentUser().getUid() != null) {
                        firebaseReference.child("user").child(mAuth.getCurrentUser().getUid()).setValue(userm);

                        Log.d("Firebase", "The account were created");
                    }else{
                        Toast.makeText(SignUpActivity.this, "Error while taking id.", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        };
        mAuth.addAuthStateListener(mAuthListener);
        register.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btn_signup:
                signIn();
                break;
        }
    }


    public void signIn(){
        nameS = name.getText().toString();
        postalAddressS = postalAddress.getText().toString();
        passwordS = password.getText().toString();
        confirmPasswordS = confirmPassword.getText().toString();
        emailS = email.getText().toString();
        phoneNumberS = phoneNumber.getText().toString();
        if(validate()){
            mAuth.createUserWithEmailAndPassword(emailS, passwordS)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d(TAG, "createUserWithEmail:success");


                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                Toast.makeText(SignUpActivity.this, "Authentication failed.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

            onSignupSuccess();
        }else{
            onSignupFailed();
            Log.d(TAG, "Sign Up validation Fail");
        }
    }


    private void onSignupSuccess() {
        register.setEnabled(true);
        setResult(RESULT_OK, null);
        finish();
    }

    private void onSignupFailed() {
        Toast.makeText(getBaseContext(), R.string.login_failed, Toast.LENGTH_LONG).show();

        register.setEnabled(true);
    }

    private boolean validate() {
        boolean valid = true;


        if (nameS.isEmpty() || nameS.length() < 3) {
            name.setError(getString(R.string.name_length_error));
            valid = false;
        } else {
            name.setError(null);
        }

        if (postalAddressS.isEmpty() || postalAddressS.length() < 3) {
            name.setError(getString(R.string.postal_length_error));
            valid = false;
        } else {
            name.setError(null);
        }
        if (phoneNumberS.isEmpty() || phoneNumberS.length() != 10) {
            phoneNumber.setError(getString(R.string.invalid_number));
            valid = false;
        } else {
            phoneNumber.setError(null);
        }

        if (emailS.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(emailS).matches()) {
            email.setError(getString(R.string.invalid_email));
            valid = false;
        } else {
            email.setError(null);
        }

        if (passwordS.isEmpty() || passwordS.length() < 4 || passwordS.length() > 10) {
            password.setError(getString(R.string.invalid_pass_length));
            valid = false;
        } else {
            password.setError(null);
        }

        if (confirmPasswordS.isEmpty() || confirmPasswordS.length() < 4
                || confirmPasswordS.length() > 10 || !(confirmPasswordS.equals(passwordS))) {
            confirmPassword.setError(getString(R.string.different_passwords_error));
            valid = false;
        } else {
            confirmPassword.setError(null);
        }

        return valid;
    }
}
