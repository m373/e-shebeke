package com.eshebeke.poocoder.EasyPost.Reference.List;


/**
 * Created by User on 25/11/2017.
 */

        import android.support.v7.widget.RecyclerView;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.TextView;

        import com.eshebeke.poocoder.EasyPost.R;

        import java.util.List;

public class PostServiceAdapter extends RecyclerView.Adapter<PostServiceAdapter.MyViewHolder> {

    private List<PostService> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            genre = (TextView) view.findViewById(R.id.genre);
            year = (TextView) view.findViewById(R.id.year);
        }
    }


    public PostServiceAdapter(List<PostService> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_raw, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        PostService postService = moviesList.get(position);
        holder.title.setText(postService.getTitle());
        holder.genre.setText(postService.getGenre());
        holder.year.setText(postService.getYear());
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
