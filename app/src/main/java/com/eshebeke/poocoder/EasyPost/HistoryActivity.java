package com.eshebeke.poocoder.EasyPost;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.eshebeke.poocoder.EasyPost.Reference.List.Parcel;
import com.eshebeke.poocoder.EasyPost.Reference.List.ParcelAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Created by poocoder on 11/22/17.
 */

public class HistoryActivity extends Fragment {
    private FirebaseAuth mAuth;
    public FirebaseAuth.AuthStateListener mAuthListener;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    ArrayList<Parcel> parcelHistorySet;
    ListView parcelHistoryList;
    ParcelAdapter parcelHistoryAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View historyView = inflater.inflate(R.layout.activity_history, null);
        mAuth = FirebaseAuth.getInstance();

        parcelHistorySet = new ArrayList<>();
        parcelHistoryList = historyView.findViewById(R.id.history_list);
        parcelHistoryAdapter = new ParcelAdapter(getActivity(), parcelHistorySet);
        parcelHistoryList.setAdapter(parcelHistoryAdapter);

        firebaseAuthSync();


        return historyView;
    }

    public void firebaseAuthSync(){
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();

        // Read from the database
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // String fbname = dataSnapshot.child("user").child("id").child("nameS").getValue(String.class);

                // This method is called once with the initial value and again
                // whenever data at this location is updated.

                if (mAuth.getCurrentUser() != null) {
                    Iterable<DataSnapshot> obtainedParcels = dataSnapshot.child("user").child(mAuth.getCurrentUser()
                            .getUid()).child("history").getChildren();

                    parcelHistorySet.clear();

                    if (!obtainedParcels.iterator().hasNext()) {
                        parcelHistoryAdapter.notifyDataSetChanged();
                    }

                    while (obtainedParcels.iterator().hasNext()) {
                        DataSnapshot ds = obtainedParcels.iterator().next();
                        ObtainedParcel ci = ds.getValue(ObtainedParcel.class);
                        try {
                            if(ds != null)
                            parcelHistorySet.add(new Parcel(ds.getKey(), ci.getDate(), !ci.getDelivery().equals("false"), ci.getPayment().equals("true")));
                        }catch (NullPointerException e){
                            Log.d("Parcel object exception", "One of the object is null");
                        }
                    }

                    parcelHistoryAdapter.notifyDataSetChanged();
                }

            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("Firebase", "Failed to read value.", error.toException());
            }
        });
    }
}
