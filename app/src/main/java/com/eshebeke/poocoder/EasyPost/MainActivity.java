package com.eshebeke.poocoder.EasyPost;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.eshebeke.poocoder.EasyPost.Reference.Dialogs.AboutDialog;
import com.eshebeke.poocoder.EasyPost.Account.LoginActivity;
import com.eshebeke.poocoder.EasyPost.Account.SignUpActivity;
import com.eshebeke.poocoder.EasyPost.Reference.PostServicesFragment;
import com.eshebeke.poocoder.EasyPost.Reference.PriceCalculatorFragment;
import com.eshebeke.poocoder.EasyPost.Reference.Dialogs.IMEIDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{


    NavigationView navigationView;
    Toolbar toolbar;
    ActionBarDrawerToggle toggle;
    DrawerLayout drawer;

    private FirebaseAuth mAuth;
    public FirebaseAuth.AuthStateListener mAuthListener;

    MenuItem nav_login, nav_signup, nav_logout, nav_history, nav_addressSearch, nav_map, nav_about;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();

        //Adding toolbar
        toolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);


        //Creation of Navigation View
        drawer = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);




        drawer.addDrawerListener(toggle);
        toggle.syncState();

        final NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        nav_login = findViewById(R.id.nav_login);
        nav_signup = findViewById(R.id.nav_sign_up);
        nav_logout = findViewById(R.id.nav_logout);
        nav_history = findViewById(R.id.nav_history);
        nav_addressSearch = findViewById(R.id.nav_address_search);
        nav_map = findViewById(R.id.nav_map);
        nav_about = findViewById(R.id.nav_about);


        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                FirebaseUser user = firebaseAuth.getCurrentUser();

                if (user == null) {
                    hideItem(navigationView, R.id.nav_logout);
                    hideItem(navigationView, R.id.nav_history);
                    hideItem(navigationView, R.id.nav_inbox);
                    showItem(navigationView, R.id.nav_sign_up);
                    showItem(navigationView, R.id.nav_login);

                } else {
                    hideItem(navigationView, R.id.nav_login);
                    hideItem(navigationView, R.id.nav_sign_up);
                    showItem(navigationView, R.id.nav_logout);
                    showItem(navigationView, R.id.nav_history);
                    showItem(navigationView, R.id.nav_inbox);
                }
            }
        };


        mAuth.addAuthStateListener(mAuthListener);



    }


    public void dataBaseLogin(FirebaseUser user) {
        // Check if user is signed in (non-null) and update UI accordingly.

        if (user != null) {
            DatabaseReference refname = FirebaseDatabase.getInstance().getReference();
            refname.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    if(mAuth.getCurrentUser().getUid() != null) {
                        String namefb = snapshot.child("user").child(mAuth.getCurrentUser().getUid()).child("nameS").getValue(String.class);
                        String emailfb = snapshot.child("user").child(mAuth.getCurrentUser().getUid()).child("emailS").getValue(String.class);


                        ((TextView) findViewById(R.id.username)).setText(namefb);
                        ((TextView) findViewById(R.id.userid)).setText(emailfb);
                    }
                }

                @Override
                public void onCancelled(DatabaseError firebaseError) {
                    Log.e("Read failed", firebaseError.getMessage());
                }
            });
        }
    }

    private void hideItem(NavigationView nv, int id) {
        Menu nav_Menu = nv.getMenu();
        nav_Menu.findItem(id).setVisible(false);
    }

    private void showItem(NavigationView nv, int id) {
        Menu nav_Menu = nv.getMenu();
        nav_Menu.findItem(id).setVisible(true);
    }



    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_inbox:
                //Inbox
                InboxFragment inboxFragment = new InboxFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, inboxFragment).commit();
                drawer.closeDrawers();
                break;
            case R.id.nav_login:
                //Signin
                startActivity(new Intent(this, LoginActivity.class));
                drawer.closeDrawers();
                break;

            case R.id.nav_logout:
                // Logout
                FirebaseAuth.getInstance().signOut();
                drawer.closeDrawers();
                break;
            case R.id.nav_sign_up:
                //Signup
                startActivity(new Intent(this, SignUpActivity.class));
                drawer.closeDrawers();
                break;
            case R.id.nav_history:
                //History
                HistoryActivity historyActivity = new HistoryActivity();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, historyActivity).commit();
                drawer.closeDrawers();
                break;
            case R.id.nav_map:
                //Map
                drawer.closeDrawers();
               break;
            case R.id.nav_about:
                //About
                AboutDialog aboutActivity = new AboutDialog();
                aboutActivity.show(getFragmentManager(), "About");
                drawer.closeDrawers();
                break;
            case R.id.nav_address_search:
                //Post Services
                PostServicesFragment postServicesFragment = new PostServicesFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, postServicesFragment).commit();
                drawer.closeDrawers();
                break;
            case R.id.nav_calc:
                // Price Calculator
                PriceCalculatorFragment priceCalculatorFragment = new PriceCalculatorFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, priceCalculatorFragment).commit();
                drawer.closeDrawers();
                break;
            case R.id.nav_imei:
                //Check IMEI
                IMEIDialog imeiDialog = new IMEIDialog();
                imeiDialog.show(getFragmentManager(), "Check your IMEI");
                drawer.closeDrawers();
                break;
        }

        return false;
    }


        //Updating db, adding indicators
}
