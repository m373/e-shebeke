package com.eshebeke.poocoder.EasyPost.Reference.List;

/**
 * Created by poocoder on 11/22/17.
 */

public class Parcel {
    private String trackingNum, deliveryDate, appointmentDate;
    private boolean changedDeliveryPlace, paidparcel;

    public Parcel(String trackingNum, String appointmentDate, boolean changedDeliveryPlace, boolean paidparcel){
        this.appointmentDate = appointmentDate;
        this.trackingNum = trackingNum;
        this.paidparcel = paidparcel;
        this.changedDeliveryPlace = changedDeliveryPlace;
    }

    public void setTrackingNum(String trackingNum) {
        this.trackingNum = trackingNum;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public void setChangedDeliveryPlace(boolean changedDeliveryPlace) {
        this.changedDeliveryPlace = changedDeliveryPlace;
    }

    public void setPaidparcel(boolean paidparcel) {
        this.paidparcel = paidparcel;
    }

    public String getTrackingNum() {
        return trackingNum;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public boolean isChangedDeliveryPlace() {
        return changedDeliveryPlace;
    }

    public boolean isPaidparcel() {
        return paidparcel;
    }
}
