package com.eshebeke.poocoder.EasyPost.Reference.Dialogs;

import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;

import com.eshebeke.poocoder.EasyPost.R;
import com.eshebeke.poocoder.EasyPost.Reference.Dialogs.DeliveryDialog;

/**
 * Created by poocoder on 11/23/17.
 */

@SuppressLint("ValidFragment")
public class ParcelActionsDialog extends DialogFragment implements View.OnClickListener{
    ImageButton shipping, payment;

    AdapterView<?> parcelParent;
    View parcelView;
    int parcelPosition;
    long parcelId;

    public ParcelActionsDialog(AdapterView<?> parent, View view, int position, long id){
        parcelView = view;
        parcelParent = parent;
        parcelPosition = position;
        parcelId = id;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle(R.string.chooseaction);
        View v = inflater.inflate(R.layout.parcel_actions, null);

        shipping = v.findViewById(R.id.ic_shipping);
        payment = v.findViewById(R.id.ic_payment);

        shipping.setOnClickListener(this);
        payment.setOnClickListener(this);



        return v;
    }

    @Override
    public void onClick(View v) {
        DeliveryDialog dpd;
        switch (v.getId()){
            case R.id.ic_payment:
                dpd = new DeliveryDialog(parcelParent, parcelView, parcelPosition, parcelId, false);
                dpd.show(getFragmentManager(), "Please pick a date");
                dismiss();
                break;

            case R.id.ic_shipping:
                dpd = new DeliveryDialog(parcelParent, parcelView, parcelPosition, parcelId ,true);
                dpd.show(getFragmentManager(), "Please pick a date");
                dismiss();
                break;
        }
    }
}
